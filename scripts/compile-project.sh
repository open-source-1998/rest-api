#!/bin/bash

# Compile typescript
npx tsc

# Move files .txt, .json, .pem etc... in ./functions
find ./src \( -name '*.txt' -o -name '*.json' -o -name '*.pem' -o -name '*.html' \) -type f -exec rsync -R {} ./functions \;
