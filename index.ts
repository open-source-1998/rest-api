class Init {

    one() {
        return {
            status: true
        };
    }

    two() {
        return {
            status: true
        };
    }

    three() {
        return {
            status: true
        };
    }

    four(data: any) {
        let state = null;
        switch (data) {
            case 'value':
                state = 1;
                break;
            case 2:
                state = 2;
                break;
            default:
                break;
        }
        return state;
    }
}
